from setuptools import setup

requirements = ['watchtower', 'boto3']

short_description = 'Virtuin remote logging service to AWS CloudWatch.'
long_description = ('Virtuin remote logging service to AWS CloudWatch.'
                    '...')

setup(name='PyVirtuinLogging',
      version='0.10.3',
      description=short_description,
      long_description=long_description,
      maintainer='Adam Page',
      maintainer_email='adam.page@samtec.com',
      url='https://apage224@bitbucket.org/samteccmd/pyvirtuinlogging.git',
      packages=['pyvirtuinlogging'],
      license='BSD',
      install_requires=requirements,
      package_data={'': ['README.md']},
      extras_require={'libev': ['pyev']},
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Natural Language :: English',
          'Operating System :: OS Independent',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: Implementation :: PyPy',
          'Topic :: Communications',
          'Topic :: Internet',
          'Topic :: Software Development :: Libraries',
          'Topic :: Software Development :: Libraries :: Python Modules',
          'Topic :: System :: Networking'],
      zip_safe=True)
