import pytest # NOQA
from pyvirtuinlogging import VirtuinLogger


class TestVirtuinLoggerConnection(object):

    def setup_method(self, test_method):
        pass

    def teardown_method(self, test_method):
        pass

    def test_open_valid_configs(self):
        logger = VirtuinLogger()
        logger.vtEnableCloudwatch = False
        configs = dict(
            testUUID='UUID of test',
            station=dict(name='debug_station'),
            test=dict(),
            dut=dict()
        )
        logger.open(configs)
        status = logger.status()
        print(status)
        assert status['open']

    def test_open_invalid_configs(self):
        logger = VirtuinLogger()
        logger.vtEnableCloudwatch = False
        configs = dict(
            notestUUID='UUID of test',
            nostation=dict(name='debug_station'),
            notest=dict(),
            nodut=dict()
        )
        logger.open(configs)
        status = logger.status()
        assert not status['open']


class TestVirtuinLoggerLogs(object):

    def setup_method(self, test_method):
        self.logger = VirtuinLogger()
        self.logger.vtEnableCloudwatch = False
        configs = dict(
            testUUID='UUID of test',
            station=dict(name='debug_station'),
            test=dict(),
            dut=dict()
        )
        self.logger.open(configs)

    def teardown_method(self, test_method):
        self.logger.close()

    def test_info_level_log(self):
        success = self.logger.log('This is a info level log.', 'info')
        assert success

    def test_warn_level_log(self):
        success = self.logger.log('This is a warn level log.', 'warn')
        assert success

    def test_error_level_log(self):
        success = self.logger.log('This is a error level log.', 'error')
        assert success

    def test_invalid_level_log(self):
        success = self.logger.log('This is an invalid level log.', 'invalid-level')
        assert not success
