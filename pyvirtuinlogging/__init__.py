"""PyVirtuinLogging exposes just one class: VirtuinLogger.
"""

__version__ = '0.10.1'

from pyvirtuinlogging.virtuinlogger import VirtuinLogger

__all__ = ['VirtuinLogger']
