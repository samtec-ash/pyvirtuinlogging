# ![Virtuin Logo](https://s3.amazonaws.com/virtuin-static-images/64x64.png) PyVirtuinLogging

## Overview

PyVirtuinLogging is used to push logs to AWS CloudWatch to monitor all aspects of active test on workstation.

The logging service logs to two transports Console (stdout/stderr) and CloudWatch.
There are 5 levels of logs:  

* error ( _highest_ )
* warn
* info
* debug
* silly ( _lowest_ )

By default, Console transport will be used for levels __debug__ and up with __debug__ and __error__ going to stderr.  
By default, CloudWatch transport will be used for levels __info__ and up.  
[Node.js](https://bitbucket.org/samteccmd/virtuinlogging#readme) version is also available.

## Getting Started:

### Prerequisites
```bash
# Install AWS cli
pip install awscli --upgrade --user

# Configure AWS
## Method 1: Prompt
aws configure
## Method 2: No Prompt
echo -ne '
[default]
aws_access_key_id = AWS_ACCESS_KEY
aws_secret_access_key = AWS_SECRET_ACCESS_KEY
' > ~/.aws/credentials
echo -ne '
[default]
region = us-east-2
output = json
' > ~/.aws/config
```

### Installation

```bash
pip install pyvirtuinlogging --upgrade
```

### Example

```python
from pyvirtuinlogging import VirtuinLogger

configs = dict(
  station=dict(name='StationDebug'),
  test=dict(testUUID='123456789'),
  dut=dict()
)

logger = VirtuinLogger()

if logger.open(configs):
  success = logger.info('Connected to CloudWatch Logs!')
  success = logger.warn('Some warning message')
  status = logger.status()
  logger.clear()
  success = logger.close()  
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/samteccmd/pyvirtuinlogging/commits/).

## Authors

* **Adam Page**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
